/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { CRMLoyaltyLevel } from './oBBEBLContractsDataCRMLoyaltyLevel';


export interface CRMLoyaltyProgram {
    Name?: string;
    Description?: string;
    AttachmentPDFName?: string;
    BackgroundImage?: string;
    BackgroundImageName?: string;
    OnlySearchesByLoyaltyRates?: boolean;
    AttachmentPDFUrl?: string;
    BackgroundImageUrl?: string;
    LoyaltyLevel?: CRMLoyaltyLevel;
}
