/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface OTALengthOfStay {
    Time?: number;

    TimeUnit?: OTALengthOfStay.TimeUnitEnum;

    MinMaxMessageType?: OTALengthOfStay.MinMaxMessageTypeEnum;

}
export namespace OTALengthOfStay {
    export enum TimeUnitEnum {
        NUMBER_0 = <any> 0
    }
    export enum MinMaxMessageTypeEnum {
        NUMBER_0 = <any> 0,
        NUMBER_1 = <any> 1
    }
}
