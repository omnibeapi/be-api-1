/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { Error } from './oBBEBLContractsResponsesError';
import { Warning } from './oBBEBLContractsResponsesWarning';


export interface DeleteEmployeeResponse {
    RequestGuid?: string;
    Errors?: Array<Error>;
    Warnings?: Array<Warning>;
    Status?: DeleteEmployeeResponse.StatusEnum;
}
export namespace DeleteEmployeeResponse {
    export type StatusEnum = 0 | 1 | 2;
    export const StatusEnum = {
        NUMBER_0: 0 as StatusEnum,
        NUMBER_1: 1 as StatusEnum,
        NUMBER_2: 2 as StatusEnum
    }
}
