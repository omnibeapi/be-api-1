core=$(cat be-api.json | jq '.version.core')
major=$(cat be-api.json | jq '.version.major')
minor=$(cat be-api.json | jq '.version.minor')
version=${core}.${major}.${minor}

let "new_minor = $minor + 1"
new_version=${core}.${major}.${new_minor}

new_api=$(cat be-api.json | jq ".version.minor=${new_minor}")
new_cmd=$(echo '.version="'${new_version}'"')
new_package=$(cat package.json | jq ${new_cmd})
echo ${new_api} > be-api.json
echo ${new_package} > package.json